FROM node:10-alpine
# FROM jutiruud/node:yarn

WORKDIR /app

COPY . .

# RUN npm config set proxy http://proxy.true.th:80 && npm config set https-proxy http://proxy.true.th:80

# RUN npm install --production
# RUN rm package-lock.json

# RUN yarn install --production

EXPOSE 3000

RUN adduser -SD appadm
USER appadm

CMD npm start
const { createLogger, format, transports } = require("winston");
var dateFormat = require('dateformat');
const package = require('../../package.json');

const additional = format(info => {
  info.tags = [process.env.env || "test", process.env.image_tag || "imageversion:local", "appversion:" + package.version];
  info["@timestamp"] = dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss.lo");
  info["@suffix"] = "nodejs-demo";
  return info;
});

const logger = createLogger({
  format: format.combine(
    // format.timestamp({
    //   format: "YYYY-MM-DDTHH:mm:ss.SSSZZ"
    // }),
    additional(),
    format.json()
  ),
  transports: [new transports.Console({ level: 'debug' })]
});

module.exports = logger;
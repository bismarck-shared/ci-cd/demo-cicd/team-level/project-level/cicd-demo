const request = require("request");
const log = require("./../logs/logger");
const LogModel = require("../logs/log-model");

var proxyUrl = "http://proxy.true.th:80/";
var proxiedRequest = request.defaults({ proxy: proxyUrl });

function get(url, param, logmodel = new LogModel()) {
  return new Promise((resolve, reject) => {
    logmodel.setRequest(url, param);
    proxiedRequest.get(`${url}?${param}`, function(err, httpResponse, body) {
      if (err) {
        logmodel.setResponse(body, httpResponse.statusCode);
        reject(err);
      }
      body = JSON.parse(body);
      logmodel.setResponse(body, httpResponse.statusCode);
      log.debug("call service", logmodel);
      resolve(body);
    });
  });
}

module.exports.get = get;

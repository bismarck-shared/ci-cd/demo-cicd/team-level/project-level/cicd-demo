let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../src/app");
let should = chai.should();
var expect = chai.expect;

var http = require("../src/services/httpService");
const Calculate = require("../src/services/calculate");
const db = require("../src/services/dbService");
chai.use(chaiHttp);

describe("GET /", () => {
  it("should return 200", done => {
    chai
      .request(server)
      .get("/")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it("probe should return 200", done => {
    chai
      .request(server)
      .get("/health")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it("log should return 200", done => {
    chai
      .request(server)
      .get("/log")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it("shoconfig should return 200", done => {

    chai
      .request(server)
      .get("/showconfig")
      .end((err, res) => {
        res.should.have.status(200);

        done();
      });
  });
  it("get should return 200", done => {
    chai
      .request(server)
      .get("/get")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("method get", () => {
  it("http", async () => {
    var rs = await http.get(
      "https://jsonplaceholder.typicode.com/todos",
      "_limit=1"
    );
    expect(rs).to.be.an("array");
  });
});

describe("Test class Calculate", () => {
  let calculate;
  beforeEach(() => {
    calculate = new Calculate();
  });
  it("should be success when value1 = 10, value2 = 15, expected 25", () => {
    // arrange
    const value1 = 10;
    const value2 = 15;
    // act
    const total = calculate.sum(value1, value2);
    // assert
    expect(25).to.be.equal(total);
  });

  it("should be success when value1 = 10, value2 = 15, expected 25", () => {
    // arrange
    const value1 = 10;
    const value2 = 15;
    // act
    const total = calculate.sum(value1, value2);
    // assert
    expect(total).to.be.an("number");
  });

  it("should be success db", () => {
    // arrange
    var rs = db.query("select 1 from dual");
    // assert
    expect(rs).is.not.null;
  });

  it("should be err db", () => {
    // arrange
    try{

      db.query("select 1 from account");
    }catch(err){
      expect(err).is.not.null;
    }
  });


  it("should be success db2", () => {
    // arrange
    var rs = db.queryProbe("select 1 from dual");
    // assert
    expect(rs).is.not.null;
  });

  it("should be success db3", () => {
    // arrange
    var rs = db.queryProbe("select :param from dual ", { param: "1" });
    // assert
    expect(rs).is.not.null;
  });
});
